# Convert URL Filter

Automatically converts internal absolute URLs into relative using text formats. Allows additional domain names to be
configured in the filter settings. Current host domain name is always considered internal.

## Usage

1. Download and install the `drupal/convert_url_filter` module. Recommended install method is composer:
   ```
   composer require drupal/convert_url_filter
   ```
2. Go to the "Text formats and editors" configuration page (/admin/config/content/formats).
3. Edit the desired text format.
4. Enable the "Convert internal absolute URLs to relative" filter.
5. Review available configurations under filter settings and save changes.
