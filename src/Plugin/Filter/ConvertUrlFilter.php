<?php

namespace Drupal\convert_url_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a filter to convert internal absolute URLs to relative.
 *
 * @Filter(
 *   id = "convert_url_filter",
 *   title = @Translation("Convert internal absolute URLs to relative"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "filter_hosts" = "",
 *   }
 * )
 */
class ConvertUrlFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $requestStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    parent::setConfiguration($configuration);
    $this->settings += ['filter_hosts' => ''];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form['filter_hosts'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Hosts'),
      '#description' => $this->t('Set additional domains to convert into relative URLs. One per line. Do NOT include http(s) or the www prefix.<br />Example: my-domain.com'),
      '#default_value' => $this->settings['filter_hosts'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $dom = Html::load($text);
    $hosts = array_filter(preg_split("(\r\n?|\n)", mb_strtolower($this->settings['filter_hosts'])));
    $hosts[] = $this->requestStack->getMainRequest()->getHttpHost();
    foreach ($dom->getElementsByTagName('a') as $link) {
      /** @var \DOMElement $link */
      if (!$link->hasAttribute('href')) {
        continue;
      }
      $path = $link->getAttribute('href');
      $url = parse_url($path);
      if (isset($url['host']) && in_array(preg_replace('/^www\./', '', mb_strtolower($url['host'])), $hosts)) {
        $link->setAttribute('href', preg_replace('/^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/\n]+)(:\d+)?/i', '', $path));
      }
    }
    return new FilterProcessResult(Html::serialize($dom));
  }

}
